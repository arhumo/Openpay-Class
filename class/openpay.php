<?php
class OpenpayB {
	public $merchant_id = '{ ID_HERE }', $dashboard_path = 'https://sandbox-dashboard.openpay.mx'; // https://dashboard.openpay.mx
	private $private_key = '{ KEY_HERE }';
	private $error, $_debug, $openpay;

	public function __construct($__MiDiaMagico=null, $_debug=false) {
		require(dirname(realpath(__FILE__)) . '/../lib/openpay/vendor/autoload.php');
		if($_debug === true) {
			$this->_debug = true;
			$this->error = null;
		} else {
			unset($this->error);
		}

		Openpay::setProductionMode(!$_debug);
		$this->openpay = Openpay::getInstance($this->merchant_id, $this->private_key, 'MX');
		return $this;
	}

	public function proteger_text($text) {
		$this->secured = strip_tags($text, "\xc2\xa0");
		$this->secured = htmlspecialchars(trim(stripslashes($this->secured)), ENT_QUOTES, 'UTF-8');

		return $this->secured;
	}

	public function get_message($property) {
    return (isset($this->$property)) ? $this->$property : null;
	}

	public function crearCliente() {
		$data = (count(func_get_args()) > 0) ? func_get_args()[0] : func_get_args();
		$nombre = (isset($data['nombre'])) ? $this->proteger_text($data['nombre']) : null;
		$apellido = (isset($data['apellido'])) ? $this->proteger_text($data['apellido']) : null;
		$email = (isset($data['email'])) ? $this->proteger_text($data['email']) : null;
		$telefono = (isset($data['telefono'])) ? $this->proteger_text($data['telefono']) : null;

		try {
			$crearCliente = array(
				'name' => $nombre,
				'last_name' => $apellido,
				'email' => $email,
				'phone_number' => $telefono
			);
			$this->crearCliente = $this->openpay->customers->add($crearCliente);
		} catch (Exception $e) {
			if($this->_debug === true) {
				try {
					if(method_exists($e,'getMessage')) $error = $e->getMessage();
				} catch (Exception $e) {
					$error = null;
				}
				$this->error = json_encode(array(
					'estado' => null,
					'mensaje'=> (!empty($error)) ? $e->getMessage() : $e,
					'location' => __METHOD__
				), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			} else {
				$this->error = json_encode(array(
					'estado' => null,
					'mensaje'=> 'Error desconocido',
				), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			}
		}
		return $this;
	}

	public function crearCargo() {
		$data = (count(func_get_args()) > 0) ? func_get_args()[0] : func_get_args();
		$source_id = (isset($data['source_id'])) ? $this->proteger_text($data['source_id']) : null;  // vacio para el modo de tienda
		$id_usuario_openpay = (isset($data['id_usuario_openpay'])) ? $this->proteger_text($data['id_usuario_openpay']) : null;
		$id_user_invitados = (isset($data['id_user_invitados'])) ? $this->proteger_text($data['id_user_invitados']) : null;
		$id_openpay = (isset($data['id_openpay'])) ? $this->proteger_text($data['id_openpay']) : null;
		$method = (isset($data['method'])) ? $this->proteger_text($data['method']) : null; // store / card
		$amount = (isset($data['amount'])) ? $this->proteger_text($data['amount']) : null;
		$description = (isset($data['description'])) ? $this->proteger_text($data['description']) : null;

		try {
			$crearCargo = array(
		    'source_id' => $source_id,
		    'method' => $method,
		    'amount' => $amount,
		    'description' => $description
		  );
			$customer = $this->openpay->customers->get($id_usuario_openpay);
			$this->crearCargo = $customer->charges->create($crearCargo);
		} catch (Exception $e) {
			if($this->_debug === true) {
				try {
					if(method_exists($e,'getMessage')) $error = $e->getMessage();
				} catch (Exception $e) {
					$error = null;
				}
				$this->error = json_encode(array(
					'estado' => null,
					'mensaje'=> (!empty($error)) ? $e->getMessage() : $e,
					'location' => __METHOD__
				), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			} else {
				$this->error = json_encode(array(
					'estado' => null,
					'mensaje'=> 'Error desconocido',
				), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			}
		}
		return $this;
	}
}
?>